import time

from apis.models import ExchangeNames
from arbitrage.arbitrage_finder import do_arbitrage
from trade.trade_context import TradeContext

start_time = time.time()


trade_context = TradeContext(ExchangeNames.BINANCE.value)
i=0
while(i<5):
    print 'LAUNCH # %s' % i
    do_arbitrage(trade_context)
    print("arbitrage :: --- %s seconds ---" % (time.time() - start_time))
    i+=1

print("BotiBot 500 :: --- %s seconds ---" % (time.time() - start_time))