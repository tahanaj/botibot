from decimal import Decimal
from multiprocessing.pool import ThreadPool

from multiprocessing import Semaphore, Condition

from apis.models import RateLimitTypes, OrderSide
from arbitrage.utils import unpack_thread_args, build_thread_args
from botibot_logger.configure import get_logger
from trade.models import ALLOCATION, MAX_PARALLEL_ORDERS, ROUTE_SIZE
from trade.utils import change_decimal_precision

logger = get_logger('ARBITRAGE-ORDER')

PARALLEL_ORDERS_SEMAPHORE = Semaphore(MAX_PARALLEL_ORDERS)

#
def apply_arbitrage(graph, trade_context):
    routes_found = sum(len(x) for x in graph.successful_routes.values())
    if routes_found == 0:
        logger.warning('NO Arbitrage Routes found - Try to decrease the safety threshold')
        return
    orders_pool = ThreadPool(routes_found)
    orders_args_list = []
    for head_asset, routes in graph.successful_routes.items():
        if not routes:
            continue

        for route in routes:
            route_notifier = Condition()
            idx = -1

            spent_amounts = {
                head_asset: trade_context.get_funds()[head_asset]['free'] * trade_context.get_risk()[ALLOCATION] / len(routes)
            }
            break_route = False
            while idx < len(route)-1 and not break_route:
                if idx == -1:
                    current_pair = (head_asset, 0)
                else:
                    current_pair = route[idx]
                order_node = build_order_dict(current_pair, idx, route, spent_amounts, trade_context)
                if not order_node:
                    logger.error('ROUTE :: {0} - ORDER not possible due qty or price non compliance to exchange {1} - ROUTE DISMISSED!'.
                                 format(route, trade_context.exchange_properties.exchange_name))
                    break
                logger.debug('ROUTE :: {0} - Current Order Node :: {1}'.format(route, order_node))
                idx += 1
                #threading.Thread(wait_for_sufficient_funds(trade_context,
                #                          order_node,
                #                          route_notifier,
                #                          spent_amounts[order_node['qty_asset']])).start()
                orders_args_list.append(build_thread_args(trade_context,
                                                          route_notifier,
                                                          spent_amounts[order_node['qty_asset']],
                                                          order_node))
    return orders_pool.map(wait_for_sufficient_funds, orders_args_list)


def build_order_dict(current_pair, current_position_in_route,route, spent_ammounts, trade_context):

    exchange_prec_qty = trade_context.exchange_properties.qty_max_decimal_precision
    exchange_prec_price = trade_context.exchange_properties.price_max_decimal_precision
    symbol = route[current_position_in_route + 1][0] + current_pair[0]
    if symbol in trade_context.get_available_pair_rich_data().keys():
        price = Decimal(1) / route[current_position_in_route + 1][1]
        order_node = {
            'symbol': symbol ,
            'qty_asset': route[current_position_in_route + 1][0],
            'price_asset': current_pair[0],
            'side': OrderSide.BUY.value
        }
        order_node['qty'] = change_decimal_precision(trade_context.
                                                     get_exchange_compliant_qty(order_node['symbol'],
                                                                                spent_ammounts[order_node['price_asset']] / price
                                                                                ),
                                                     exchange_prec_qty)
        order_node['price'] = change_decimal_precision(trade_context.get_exchange_compliant_price(order_node['symbol'],
                                                                                                price),
                                                       exchange_prec_price)
        if not order_node['qty'] or not order_node['price']:
            return None
        spent_ammounts[order_node['price_asset']] = order_node['qty'] * order_node['price']
        spent_ammounts[order_node['qty_asset']] = order_node['qty']
    else:
        price = route[current_position_in_route + 1][1]
        symbol = current_pair[0] + route[current_position_in_route + 1][0]
        order_node = {
            'symbol': symbol,
            'price_asset': route[current_position_in_route + 1][0],
            'qty_asset': current_pair[0],
            'side': OrderSide.SELL.value,
        }
        order_node['qty'] = change_decimal_precision(trade_context.get_exchange_compliant_qty(order_node['symbol'],
                                                                    spent_ammounts[current_pair[0]]),
                                                     exchange_prec_qty)
        order_node['price'] = change_decimal_precision(trade_context.get_exchange_compliant_price(order_node['symbol'],
                                                                                                    price),
                                                       exchange_prec_price)
        if not order_node['qty'] or not order_node['price']:
            return None
        spent_ammounts[current_pair[0]] = order_node['qty']
        spent_ammounts[order_node['price_asset']] = order_node['qty'] * order_node['price']

    if not trade_context.is_order_notional_exchange_compliant(order_node['symbol'],
                                                                         order_node['price'],
                                                                         order_node['qty']):
        order_node = None
    return order_node


def wait_for_sufficient_funds(args):
    trade_context, route_notifier, amount_to_be_spent, order_node = unpack_thread_args(args)
    route_notifier.acquire()
    notif_count = 1
    while not trade_context.sufficient_funds(order_node['qty_asset'], amount_to_be_spent) \
            and notif_count <= trade_context.get_risk()[ROUTE_SIZE]:
        route_notifier.wait(10)
        notif_count += 1
        logger.debug('NOTIFICATION RECIEVED')
    if notif_count > trade_context.get_risk()[ROUTE_SIZE]:
        return 'NO FUNDS', order_node
    status = place_safe_order(trade_context, order_node, route_notifier, amount_to_be_spent)
    route_notifier.release()
    return status, order_node


def place_safe_order(trade_context, order_node, notifier, amount_to_be_spent):
    PARALLEL_ORDERS_SEMAPHORE.acquire()
    notifier.acquire()
    trade_context.random_limit_consumption_check(RateLimitTypes.ORDERS.value)
    trade_context.lock_funds(order_node['qty_asset'], amount_to_be_spent)
    response, order_timestamp = trade_context.place_order(symbol=order_node['symbol'],
                                                          side=order_node['side'],
                                                          qty=order_node['qty'],
                                                          price=order_node['price'])
    trade_context.wait_until_order_finishes(order_timestamp)
    PARALLEL_ORDERS_SEMAPHORE.release()
    order_status_tuple = trade_context.check_order_response(response)
    logger.debug('ORDER {0} finished with status {1}'.format(order_node, order_status_tuple))
    if order_status_tuple[0]:
        trade_context.debit_funds(order_node['qty_asset'], amount_to_be_spent)
        trade_context.credit_funds(order_node['price_asset'], order_node['qty'])
        for comm_asset, comm in order_status_tuple[1].items():
            trade_context.debit_funds(comm_asset, comm)
        notifier.notify()

    notifier.release()
    return order_status_tuple[0]

