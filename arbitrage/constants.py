from decimal import Decimal

SAFETY_THRESHOLD = Decimal(0.02)
BASE_ASSETS = ['BTC', 'ETH', 'BNB']