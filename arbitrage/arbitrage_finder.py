from collections import defaultdict
from decimal import Decimal

from apis.models import RateLimitTypes
from arbitrage.constants import SAFETY_THRESHOLD, BASE_ASSETS
from arbitrage.order import apply_arbitrage
from botibot_logger.configure import get_logger
from trade.models import ROUTE_SIZE

logger = get_logger('ARBITRAGE-FINDER')


class Graph(object):
    def __init__(self):
        self.successful_routes = {}
        self.price_matrix = defaultdict(dict)
        self.pricer_assets = set()

    def __repr__(self):
        return "GRAPH !!"

    def __str__(self):
        return "PRICE MATRIX : {0} \n - SUCCESSFUL ROUTES {1}".\
            format(self.price_matrix, self.successful_routes if self.successful_routes else 'not yet')


def do_arbitrage(trade_context=None):
    if not trade_context:
        return

    graph = build_price_matrix(trade_context)
    find_arbitrage_opportunities(graph, BASE_ASSETS)
    return apply_arbitrage(graph, trade_context)


def build_price_matrix(trade_context):
    graph = Graph()
    graph.risk = trade_context.get_risk()
    trade_context.random_limit_consumption_check(RateLimitTypes.REQUESTS.value, 20)
    available_pair_data = trade_context.get_available_pair_rich_data()
    prices = trade_context.refresh_all_prices()

    for price_dict in prices:
        if available_pair_data[price_dict['symbol']]['status'] != trade_context.exchange_properties.symbol_trading_status_ok:
            continue
        qty_asset = available_pair_data[price_dict['symbol']]['base']
        pricer_asset = available_pair_data[price_dict['symbol']]['quote']
        graph.pricer_assets.add(pricer_asset)
        graph.price_matrix[pricer_asset][qty_asset] = Decimal(Decimal(1)/Decimal(price_dict['price']))
        graph.price_matrix[qty_asset][pricer_asset] = Decimal(price_dict['price'])
    return graph


def find_arbitrage_opportunities_from_head_recursive(graph, head_asset):
    for qty_asset, price in graph.price_matrix[head_asset].items():
        if qty_asset != graph.head_asset:
            if len(graph.route) > graph.risk[ROUTE_SIZE]:
                break
            graph.normalized_sum *= price
            graph.route[qty_asset] = price
            find_arbitrage_opportunities_from_head_recursive(graph, qty_asset)
        elif graph.normalized_sum + price > 1 + SAFETY_THRESHOLD:
            print 'found route: {0} with price {1}'.format(graph.route, graph.normalized_sum)
        else:
            continue
    if head_asset != graph.head_asset:
        graph.normalized_sum -= graph.price_matrix[graph.head_asset][head_asset]
        graph.route.pop(head_asset, None)


def find_arbitrage_opportunities_from_head_iterative(graph, head_asset):
    cycles = list(find_cycles(graph, head_asset))
    graph.successful_routes[head_asset] = []
    for cycle in cycles:
        overall_price = 1
        for pair in cycle:
            overall_price *= pair[1]
        if overall_price > 1 + SAFETY_THRESHOLD:
            graph.successful_routes[head_asset].append(cycle)


def find_cycles(graph, start):
    fringe = [(start, [])]
    while fringe:
        state, path = fringe.pop()
        if path and len(path) > graph.risk[ROUTE_SIZE]:
            continue
        if path and state == start:
            yield path
            continue
        for qty_asset, price in graph.price_matrix[state].items():
            if qty_asset in path:
                continue
            fringe.append((qty_asset, path + [(qty_asset, price)]))


def find_arbitrage_opportunities(graph, funds):
    for asset in funds:
        graph.head_asset = asset
        find_arbitrage_opportunities_from_head_iterative(graph, asset)
    logger.debug('ARBITRAGE GRAPH STATE : {0}'.format(graph))