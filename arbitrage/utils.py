from enum import Enum


class OrderThreadArgs(Enum):
    CONTEXT = 'current_context'
    NOTIF = 'same_route_notification'
    SPENT = 'amount_spent'
    ORDER = 'order_node'

def unpack_thread_args(args):
    return args[OrderThreadArgs.CONTEXT.value], \
           args[OrderThreadArgs.NOTIF.value], \
           args[OrderThreadArgs.SPENT.value], \
           args[OrderThreadArgs.ORDER.value]


def build_thread_args(trade_context, route_notifier, spent_amount, order_node):
    return {OrderThreadArgs.CONTEXT.value: trade_context,
             OrderThreadArgs.ORDER.value: order_node,
             OrderThreadArgs.NOTIF.value: route_notifier,
             OrderThreadArgs.SPENT.value: spent_amount}