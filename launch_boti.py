import threading
import time

from apis.models import ExchangeNames
from arbitrage.arbitrage_finder import do_arbitrage
from trade.trade_context import TradeContext

start_time = time.time()


trade_context = TradeContext(ExchangeNames.BINANCE.value)
i=1
while(i <= 5):
    print 'LAUNCH # %s' % i
    threading.Thread(do_arbitrage(trade_context)).start()
    i+=1

print("BotiBot 500 :: --- %s seconds ---" % (time.time() - start_time))