from enum import Enum


class BinanceEndpoints(Enum):
    BASE = 'https://api.binance.com'
    SERVER_TIME = BASE + '/api/v1/time'
    EXCHANGE_INFOS = BASE + '/api/v1/exchangeInfo'
    LAST_X_TRADES = BASE + '/api/v1/trades'
    PRICE_TICKERS = BASE + '/api/v3/ticker/price'
    BEST_BOOK_TICKERS = BASE + '/api/v3/ticker/bookTicker'
    ORDER = BASE + '/api/v3/order'
    TEST_ORDER = BASE + '/api/v3/order/test'
    ALL_ORDERS = BASE + '/api/v3/allOrders'
    OPEN_ORDERS = BASE + '/api/v3/openOrders'
    ACCOUNT_INFOS = BASE + '/api/v3/account'


ENDPOINT_WEIGHTS = {
    BinanceEndpoints.EXCHANGE_INFOS : 1,
    BinanceEndpoints.LAST_X_TRADES:1,
    BinanceEndpoints.PRICE_TICKERS:1,
    BinanceEndpoints.BEST_BOOK_TICKERS:1,
    BinanceEndpoints.ORDER:1,
    BinanceEndpoints.TEST_ORDER:1,
    BinanceEndpoints.ALL_ORDERS:1,
    BinanceEndpoints.OPEN_ORDERS:1,
    BinanceEndpoints.ACCOUNT_INFOS:1,
}


class OrderRespType(Enum):
    SIMPLE_ACK = 'ACK'
    RESULT = 'RESULT'
    FULL_RESULT = 'FULL'


class OrderStatus(Enum):
    FILLED = 'FILLED'
