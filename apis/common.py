from abc import ABCMeta, abstractmethod


class CommonApiConsumer():
    """
    Abstract class to serve as a template for exchange apis consumers.
    Each method must returns the consumed resource (for limits) + the requested data
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, base_endpoint):
        self.base_endpoint = base_endpoint

    @abstractmethod
    def get_exchange_params(self):
        """ returns requests QTY, dict of exchange apis calls limits, dict of available trading pairs"""
        pass

    @abstractmethod
    def get_prices(self, pair=None):
        pass

    @abstractmethod
    def get_all_prices(self):
        pass

    @abstractmethod
    def place_order(self,
                    symbol,
                    side,
                    type,
                    qty,
                    recv_window=None,
                    price=None,
                    time_in_force=None,
                    new_client_order_id=None,
                    iceberg_qty=None,
                    new_order_resp_type=None,
                    test=True):
        pass

    @abstractmethod
    def get_funds(self, recv_window=None):
        pass