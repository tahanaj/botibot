from enum import Enum


# Every property which definition or nomenclature changes from and exchange to another should be defined here
class ExchangePropertiesFactory(object):
    def __init__(self, exchange_name):
        self.exchange_name = exchange_name
        if exchange_name == ExchangeNames.BINANCE.value:
            self.class_name = 'Binance'
            self.symbol_trading_status_ok = 'TRADING'
            self.default_recv_window = 5000
            self.qty_max_decimal_precision = 18
            self.price_max_decimal_precision = 18
            try:
                from binance_models import OrderRespType, OrderStatus
                self.order_resp_types_enum = OrderRespType
                self.order_status_enum = OrderStatus
            except:
                print 'Please define OrderRespType and OrderStatus Enums for exchange %s' % exchange_name
        else:
            raise UnknownExchange(exchange_name)

    def __repr__(self):
        return '%s exchange !!' % self.exchange_name

    def __str__(self):
        return 'The exchange %s with API consumer class name %s !!' %(self.exchange_name, self.class_name)


class RateLimitTypes(Enum):
    ORDERS = 'ORDERS'
    REQUESTS = 'REQUESTS'


class TimeInterval(Enum):
    SECOND = 'SECOND'
    MINUTE = 'MINUTE'
    DAY = 'DAY'
    HOUR = 'HOUR'


class ExchangeNames(Enum):
    BINANCE = 'binance'
    #KRAKEN = 'kraken'


class OrderTypes(Enum):
    LIMIT = 'LIMIT'
    MARKET = 'MARKET'
    STOP_LOSS = 'STOP_LOSS'
    TAKE_PROFIT = 'TAKE_PROFIT'


#TODO implement a fill or kill mechanism on the entire arbitrage route (killed orders are retried with GTC)
class TimeInForce(Enum):
    GOOD_TILL_CANCELED = 'GTC' #orders stays until it gets canceled
    IMMEDIATE_OR_CANCEL = 'IOC' # all parts of the order that can be filled immediately are, all else are canceled
    FILL_OR_KILL = 'FOK' #order is filled immediately entirely or killed if not possible


class OrderSide(Enum):
    SELL = 'SELL'
    BUY = 'BUY'

class QueryFailed(Exception):
    def __init___(self, endpoint, code, content):
        Exception.__init__(self,"Query to %s failed with status code %s and error message :: %s " % (endpoint, code, content))


class UnknownExchange(Exception):
    def __init___(self, exchange_name):
        Exception.__init__(self,"The exchange %s is unknown. Try to implement its API !! " % exchange_name)


BID = 'bid'
ASK = 'ask'

