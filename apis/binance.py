import hashlib
import hmac
import json
import time
from collections import defaultdict

import requests

from apis.binance_models import BinanceEndpoints, ENDPOINT_WEIGHTS, OrderRespType
from apis.common import CommonApiConsumer
from apis.models import BID, ASK, OrderTypes, TimeInForce, QueryFailed
from botibot_logger.configure import get_logger
from settings.settings import binance_key, binance_secret, TEST_ENV

logger = get_logger('API-BINANCE')

SYMBOL_MIN_NOTIONAL_FILTER = 'MIN_NOTIONAL'

SYMBOL_PRICE_FILTER = 'PRICE_FILTER'

SYMBOL_QTY_FILTER = 'LOT_SIZE'

RESPONSE_OK = 200


class Binance(CommonApiConsumer):
    def __init__(self):
        super(Binance, self).__init__(BinanceEndpoints.BASE.value)
        self.headers = {}

    def get_signature(self, query_string):
        if binance_secret:
            return hmac.new(binance_secret, msg=query_string, digestmod=hashlib.sha256).hexdigest()
        else:
            logger.error('Binance secret key not provided')

    def add_key_header(self):
        if binance_key:
            self.headers['X-MBX-APIKEY'] = binance_key
        else:
            logger.error('Binance Key not provided !')

    def get_server_time(self):
        r = requests.get(BinanceEndpoints.SERVER_TIME.value)
        return json.loads(r.text)['serverTime']

    def get_exchange_params(self):
        limits = defaultdict(dict)
        symbol_data = defaultdict(dict)
        r = requests.get(BinanceEndpoints.EXCHANGE_INFOS.value)
        response = json.loads(r.text)
        if r.status_code != RESPONSE_OK:
            raise QueryFailed(BinanceEndpoints.EXCHANGE_INFOS.value, r.status_code,
                              response['code'] + '-' + response['msg'])
        for rate_limit_dict in response['rateLimits']:
            limits[rate_limit_dict['rateLimitType']][rate_limit_dict['interval']] = rate_limit_dict['limit']
        logger.debug('EXCHANGE INFO - symbols data  :: {0}'.format(response['symbols']))
        for symbols in response['symbols']:
            symbol_data[symbols['symbol']] = {
                'base':symbols['baseAsset'],
                'quote': symbols['quoteAsset'],
                'order_types': symbols['orderTypes'],
                'status': symbols['status'],
            }
            symbol_data['asset_precision'][symbols['baseAsset']] = symbols['baseAssetPrecision']
            symbol_data['asset_precision'][symbols['quoteAsset']] = symbols['quotePrecision']
            for filter in symbols['filters']:
                if filter['filterType'] == SYMBOL_QTY_FILTER:
                    symbol_data[symbols['symbol']]['min_qty'] = filter['minQty']
                    symbol_data[symbols['symbol']]['max_qty'] = filter['maxQty']
                    symbol_data[symbols['symbol']]['qty_step'] = filter['stepSize']
                elif filter['filterType'] == SYMBOL_PRICE_FILTER:
                    symbol_data[symbols['symbol']]['min_price'] = filter['minPrice']
                    symbol_data[symbols['symbol']]['max_price'] = filter['maxPrice']
                    symbol_data[symbols['symbol']]['price_step'] = filter['tickSize']
                elif filter['filterType'] == SYMBOL_MIN_NOTIONAL_FILTER :
                    symbol_data[symbols['symbol']]['min_notional'] = filter['minNotional']

        return ENDPOINT_WEIGHTS[BinanceEndpoints.EXCHANGE_INFOS], limits, symbol_data

    def get_all_prices(self):
        return self.get_prices()

    def get_prices(self, pair=None):
        params = {}
        if pair:
            params['symbol'] = pair
        r = requests.get(BinanceEndpoints.PRICE_TICKERS.value, data=json.dumps(params))
        response = json.loads(r.text)
        if r.status_code != RESPONSE_OK:
            raise QueryFailed(BinanceEndpoints.PRICE_TICKERS.value, r.status_code,
                              response['code'] + '-' + response['msg'])
        if not isinstance(response, list):
            response = [response]
        return ENDPOINT_WEIGHTS[BinanceEndpoints.PRICE_TICKERS], response

    def get_all_best_book_tickers(self):
        return self.get_book_tickers()

    def get_book_tickers(self, pair=None):
        best_book_tickers = defaultdict(dict)
        params = {}
        if pair:
            params['symbol'] = pair
        r = requests.get(BinanceEndpoints.BEST_BOOK_TICKERS.value, data=json.dumps(params))
        response = json.loads(r.text)
        if r.status_code != RESPONSE_OK:
            raise QueryFailed(BinanceEndpoints.BEST_BOOK_TICKERS.value, r.status_code,
                              response['code'] + '-' + response['msg'])
        if not isinstance(response, list):
            response = [response]
        for best_book_dict in response:
            best_book_tickers[best_book_dict['symbol']][BID] = {
                'price': best_book_dict['bidPrice'],
                'qty': best_book_dict['bidQty']
            }
            best_book_tickers[best_book_dict['symbol']][ASK] = {
                'price': best_book_dict['bidPrice'],
                'qty': best_book_dict['bidQty']
            }
        logger.debug('BEST BOOK TICKERS - RESPONSE :: {0}'.format(response))
        return ENDPOINT_WEIGHTS[BinanceEndpoints.BEST_BOOK_TICKERS], best_book_tickers

    def place_order(self,
                    symbol,
                    side,
                    type,
                    qty,
                    recv_window=None,
                    price=None,
                    time_in_force=TimeInForce.FILL_OR_KILL.value,
                    new_client_order_id=None,
                    iceberg_qty=None,
                    new_order_resp_type=OrderRespType.FULL_RESULT.value,
                    test=TEST_ENV):
        

        if type == OrderTypes.LIMIT.value and (not time_in_force or not qty or not price):
            logger.error('LIMIT order missing required parameter !')
            return
        elif type == OrderTypes.MARKET.value and not qty:
            logger.error('MARKET order missing required parameter !')
            return

        if test:
            endpoint = BinanceEndpoints.TEST_ORDER
        else:
            endpoint = BinanceEndpoints.ORDER

        self.add_key_header()

        params = {}
        query_string = 'symbol='+symbol+'&side='+side+'&type='+type
        query_string += '&recvWindow='+str(recv_window) if recv_window else ''
        query_string += '&timeInForce=' + time_in_force if time_in_force else ''
        query_string += '&quantity=' + str(qty) if qty else ''
        query_string += '&price=' + str(price) if price else ''
        query_string += '&newClientOrderId=' + new_client_order_id if new_client_order_id else ''
        query_string += '&icebergQty=' + iceberg_qty if iceberg_qty else ''
        query_string += '&newOrderRespType=' + new_order_resp_type if new_order_resp_type else ''

        for piece in query_string.split('&'):
            keyv = piece.split('=')
            params[keyv[0]] = keyv[1]

        #weird server time is ahead
        timestamp = int(round(time.time()*1000))
        params['timestamp'] = str(timestamp)
        query_string += '&timestamp='+str(timestamp)

        signature = self.get_signature(query_string)
        params['signature'] = signature

        r = requests.post(endpoint.value, params=params, headers=self.headers)
        logger.debug("ORDER - PARAMS :: {0}".format(params))

        response = json.loads(r.text)
        logger.debug("ORDER - RESPONSE :: {0}".format(response))

        #if r.status_code != RESPONSE_OK:
        #    raise QueryFailed(endpoint.value, str(r.status_code), str(response['code'])+'-'+response['msg'])
        return ENDPOINT_WEIGHTS[endpoint], self.process_order_response(response, new_order_resp_type), timestamp

    def process_order_response(self, response, response_type):
        if response_type == OrderRespType.SIMPLE_ACK.value:
            return {'order_id' : response['orderId'],
                    'client_order_id' : response['clientOrderId'],
                    'status' : '',
                    'execution_rate' : 0.0,
                    'commision_dict' : {},
                    }
        elif response_type == OrderRespType.RESULT.value:
            return {'order_id' : response['orderId'],
                    'client_order_id' : response['clientOrderId'],
                    'status' : response['status'],
                    'execution_rate' : float(response['executedQty'])/float(response['origQty']),
                    'commision_dict' : {},
                    }
        elif response_type == OrderRespType.FULL_RESULT.value:
            commissions = defaultdict(float)
            for fill_dict in response['fills']:
                commissions[fill_dict['commissionAsset']] += float(fill_dict['commission'])
            return {'order_id' : response['orderId'],
                    'client_order_id' : response['clientOrderId'],
                    'status' : response['status'],
                    'execution_rate' : float(response['executedQty'])/float(response['origQty']),
                    'commision_dict' : commissions,
                    }

    def get_funds(self, recv_window=None):
        self.add_key_header()
        params = {}
        timestamp = int(round(time.time() * 1000))
        query_string='timestamp='+str(timestamp)
        query_string += '&recvWindow=' + str(recv_window) if recv_window else ''
        for piece in query_string.split('&'):
            keyv = piece.split('=')
            params[keyv[0]] = keyv[1]

        signature = self.get_signature(query_string)
        params['signature'] = signature

        r = requests.get(BinanceEndpoints.ACCOUNT_INFOS.value, params=params, headers=self.headers)
        response = json.loads(r.text)
        if r.status_code != RESPONSE_OK:
            raise QueryFailed(BinanceEndpoints.ACCOUNT_INFOS.value, str(r.status_code), str(response['code'])+'-'+response['msg'])
        return ENDPOINT_WEIGHTS[BinanceEndpoints.ACCOUNT_INFOS], response['balances'], timestamp
