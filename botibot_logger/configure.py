import logging
from settings.settings import DEBUG, LOG_FILE_PATH

if DEBUG:
    level = logging.DEBUG
else:
    level = logging.INFO

def get_logger(app_name):
    logging.basicConfig(level=level)
    hdlr = logging.FileHandler(LOG_FILE_PATH)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger = logging.getLogger(app_name)
    logger.addHandler(hdlr)
    return logger