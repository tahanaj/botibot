# README #

![alt text](https://i.imgur.com/m7lzLQ2.gif)

Fat (botti) arbitrage bot for binance and other crypto exchanges.

# Setup #
* generate access key and secret key from binance 
* create user_settings.py under settings
* paste key into binance_key constant and secret into binance_secret constant 
	
# Run #
* install redis server [https://redis.io/topics/quickstart]
* launch redis: redis-server
* use the launcher file: 
```sh	
python stress_test_boti.py 
```
	
	Ultimately launch_boti.py will be used for run
	
# Exchanges #
* Binance [https://www.binance.com] - API [https://github.com/binance-exchange/binance-official-api-docs]

# BackLog #
* Go check [https://bitbucket.org/botibot/botibot/addon/bitbucket-trello-addon/trello-board]
