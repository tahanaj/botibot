from decimal import ROUND_DOWN, getcontext, Decimal, ROUND_HALF_EVEN

from trade.models import DEFAULT_DECIMAL_PRECISION


def change_decimal_precision(value, prec, rounding_method=ROUND_DOWN):
    """Force the decimal precision to change for a given number ROUND_DOWN is advised when handling trade quantities and prices"""
    if value is None or not prec:
        return None
    getcontext().rounding = rounding_method
    getcontext().prec = prec
    new_val = Decimal(value) + Decimal(0)
    getcontext().prec = DEFAULT_DECIMAL_PRECISION
    getcontext().rounding = ROUND_HALF_EVEN
    return new_val