import importlib
import random
import time
from collections import defaultdict
from datetime import datetime, timedelta
from decimal import Decimal

from apis.models import RateLimitTypes, TimeInterval, ExchangePropertiesFactory, TimeInForce, OrderTypes
from botibot_logger.configure import get_logger
from trade.models import RiskProfiles, ORDER_LIFE_TIME, DEFAULT_DECIMAL_PRECISION
from trade.resource_manager import update_resource_cache, generate_resource_cache_keys, get_consumed_resources
from trade.utils import change_decimal_precision

logger = get_logger('TRADE-CONTEXT')


class TradeContext:
    def __init__(self, exchange_name, risk=RiskProfiles.LOW.value):
        self.exchange_properties = ExchangePropertiesFactory(exchange_name)
        module = importlib.import_module('apis.%s' % self.exchange_properties.exchange_name)
        self.exchange_instance = getattr(module, self.exchange_properties.class_name)()
        self.set_exchange_infos()
        self.funds = defaultdict(dict)
        self.risk = risk

    def get_exchange_instance(self):
        if not self.exchange_instance:
            module = __import__(self.exchange_properties.exchange_name)
            self.exchange_instance = getattr(module, self.exchange_properties.class_name)()
        return self.exchange_instance

    def increment_consumed_limits(self, limit_type, value=1):
        for interval in self.get_exchange_limits()[limit_type].keys():
            update_resource_cache(self.exchange_properties.exchange_name, limit_type, interval, value)

    def limit_reached_lock(self, limit_type):
        current_date = datetime.utcnow()
        consumed_resources = get_consumed_resources(list(generate_resource_cache_keys([self.exchange_properties.exchange_name],
                                                                                      [limit_type],
                                                                                      self.get_exchange_limits()[limit_type].keys(),
                                                                                      current_date)))
        lock_value = {
            0 :  0,
            1: ((current_date + timedelta(seconds=1)).replace(microsecond=0) - current_date).total_seconds(),
            2: ((current_date + timedelta(minutes=1)).replace(second=0,
                                                              microsecond=0) - current_date).total_seconds(),
            3: ((current_date + timedelta(hours=1)).replace(minute=0,
                                                            second=0,
                                                            microsecond=0) - current_date).total_seconds(),
            4: ((current_date + timedelta(days=1)).replace(hour=0,
                                                            minute=0,
                                                            second=0,
                                                            microsecond=0) - current_date).total_seconds()
        }
        lock_intensity = {
            TimeInterval.SECOND.value:1,
            TimeInterval.MINUTE.value:2,
            TimeInterval.HOUR.value:3,
            TimeInterval.DAY.value:4
        }
        lock_intensity_val = 0
        for key, value in consumed_resources.items():
            key_list = key.split('-')
            if value >= self.get_exchange_limits()[limit_type][key_list[2]]:
                lock_intensity_val = max(lock_intensity_val, lock_intensity[key_list[2]])
        logger.debug('LIMIT REACHED - SLEEP TIME - {0}'.format(lock_value[lock_intensity_val]))
        time.sleep(lock_value[lock_intensity_val])


    def random_limit_consumption_check(self, limit_type, x=5):
        """checks resource consumption 1 out of x API calls (ish).
        Not very beautiful but better than a call to limit reached check ervery time """
        if random.randint(1,x) == 1:
            self.limit_reached_lock(limit_type)

    def set_exchange_infos(self):
        weight, self.exchange_limits, self.available_pair_data = self.exchange_instance.get_exchange_params()
        self.increment_consumed_limits(RateLimitTypes.REQUESTS.value, weight)

    def get_available_pair_rich_data(self):
        return self.available_pair_data

    def get_exchange_limits(self):
        return self.exchange_limits

    def get_exchange_compliant_values(self, symbol, value, value_type):
        value = Decimal(value)
        min_val = Decimal(self.get_available_pair_rich_data().get(symbol, {}).get('min_%s'%value_type, -1))
        max_val = Decimal(self.get_available_pair_rich_data().get(symbol, {}).get('max_%s'%value_type, -1))
        min_step = Decimal(self.get_available_pair_rich_data().get(symbol, {}).get('%s_step'%value_type, -1))
        if min_val == Decimal(-1) or max_val == Decimal(-1) or min_step == Decimal(-1):
            return value
        if value >= min_val and value <= max_val:
            steps = int((value - min_val)/min_step)
            compliant_value = min_val + steps*min_step
        elif value <= min_val:
            compliant_value = min_val
        else:
            compliant_value = max_val
        return compliant_value

    def get_exchange_compliant_price(self, symbol, price):
        return self.get_exchange_compliant_values(symbol, price, 'price')

    def get_exchange_compliant_qty(self, symbol, qty):
        return self.get_exchange_compliant_values(symbol, qty, 'qty')

    def is_order_notional_exchange_compliant(self, symbol, price, qty):
        min_notional = Decimal(self.get_available_pair_rich_data().get(symbol, {}).get('min_notional', 0))
        if price * qty >= min_notional:
            return True
        return False

    def refresh_all_prices(self):
        weight, prices = self.get_exchange_instance().get_all_prices()
        self.increment_consumed_limits(RateLimitTypes.REQUESTS.value, weight)
        return prices

    def refresh_price(self, pair):
        weight, price = self.get_exchange_instance().get_price(pair)
        self.increment_consumed_limits(RateLimitTypes.REQUESTS.value, weight)
        return price

    def refresh_funds(self):
        weight, funds, timestamp = self.get_exchange_instance().get_funds(self.get_risk()[ORDER_LIFE_TIME])
        self.increment_consumed_limits(RateLimitTypes.REQUESTS.value, weight)
        for asset_dict in funds:
            asset_min_qty = int(self.get_available_pair_rich_data().get('asset_precision', {}).
                                get(asset_dict['asset'],DEFAULT_DECIMAL_PRECISION))
            if Decimal(asset_dict['free']) != Decimal(0):
                self.funds[asset_dict['asset']]['free'] = change_decimal_precision(Decimal(asset_dict['free']),
                                                                                   asset_min_qty)
                if asset_dict.get('locked', 0):
                    self.funds[asset_dict['asset']]['locked'] =  change_decimal_precision(Decimal(asset_dict.get('locked', 0)),
                                                                                                    asset_min_qty)
                else:
                    self.funds[asset_dict['asset']]['locked'] = change_decimal_precision(Decimal(0), asset_min_qty)

        logger.debug('FUNDS - FUNDS - {0}'.format(self.funds))
        return self.funds

    def get_funds(self):
        if not self.funds:
            self.refresh_funds()
        return self.funds

    def lock_funds(self, asset, qty):
        self.funds[asset]['free'] -= qty
        self.funds[asset]['locked'] += qty

    def free_funds(self, asset, qty):
        self.funds[asset]['free'] += qty
        self.funds[asset]['locked'] -= qty

    def debit_funds(self, asset, qty):
        self.funds[asset]['locked'] -= qty

    def credit_funds(self, asset, qty):
        self.funds[asset]['free'] += qty

    def sufficient_funds(self, asset, qty):
        if self.funds.get(asset, {}).get('free', 0) >= qty:
            return True
        return False

    def place_order(self, symbol, side, qty, price, recv_window=None, time_in_force=TimeInForce.FILL_OR_KILL.value):
        if not recv_window:
            recv_window = self.risk[ORDER_LIFE_TIME]
        weight, response, timestamp = self.get_exchange_instance().place_order(symbol,
                                                                               side,
                                                                               OrderTypes.LIMIT.value,
                                                                               qty,
                                                                               recv_window=recv_window,
                                                                               price=price,
                                                                               time_in_force=time_in_force)
        self.increment_consumed_limits(RateLimitTypes.ORDERS.value, weight)
        return response, timestamp

    def check_order_response(self, response, time_in_force=TimeInForce.FILL_OR_KILL.value):
        if response['status'] == self.exchange_properties.order_status_enum.FILLED.value:
            if time_in_force == TimeInForce.FILL_OR_KILL.value:
                return (True, response['commission_dict'], 1)
            return (True, response['commission_dict'], response['execution_rate'])
        return (False, {}, 0)

    def is_order_finished(self, order_timestamp, order_recv_window=None):
        if not order_recv_window:
            order_recv_window = self.risk[ORDER_LIFE_TIME]

        if (time.time()*1000) - order_timestamp >= order_recv_window:
            return True
        return False

    def wait_until_order_finishes(self, order_timestamp, recv_window=None, period=0.01):
        if not recv_window:
            recv_window = self.risk[ORDER_LIFE_TIME]
        must_end = (time.time()*1000) + recv_window
        while (time.time()*1000) < must_end:
            if self.is_order_finished(order_timestamp, recv_window):
                return
            time.sleep(period)

    def get_risk(self):
        return self.risk

    def set_risk(self, risk):
        self.risk = risk