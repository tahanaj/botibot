import datetime
import pickle

import redis

from apis.models import TimeInterval, RateLimitTypes
from botibot_logger.configure import get_logger
from settings.settings import RESOURCE_CACHE_URL

CACHE = redis.StrictRedis(host=RESOURCE_CACHE_URL)

logger = get_logger('TRADE-RESOURCE-MANAGER')


def update_resource_cache(exchange_name, limit_type, interval, resource):
    current_date = datetime.datetime.utcnow()
    if interval == TimeInterval.SECOND.value:
        time_id = current_date.second
        expiry = current_date.replace(microsecond=0) + datetime.timedelta(seconds=1)
    elif interval == TimeInterval.MINUTE.value:
        time_id = current_date.minute
        expiry = current_date.replace(second=0, microsecond=0) + datetime.timedelta(minutes=1)
    elif interval == TimeInterval.HOUR.value:
        time_id = datetime.datetime.utcnow().hour
        expiry = current_date.replace(minute=0,second=0, microsecond=0) + datetime.timedelta(hours=1)
    else:
        time_id = datetime.datetime.utcnow().weekday()
        expiry = current_date.replace(hour = 0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1)

    key = exchange_name+'-' + limit_type + '-' + interval + '-' + str(time_id)
    previous_resource = CACHE.get(key)
    p_r = 0
    if previous_resource:
        p_r = pickle.loads(previous_resource)
    logger.debug('UPDATE CACHE - key {2} - PAST {0} - TO BE ADDED {1}'.format(p_r, resource, key))
    ex = int((expiry - current_date).total_seconds())
    value = pickle.dumps(resource + p_r)
    CACHE.set(key, value, ex=ex)


def get_consumed_resources(keys):
    consumed = {}
    for key in keys:
        resource = CACHE.get(key)
        r = 0
        if resource:
            r = pickle.loads(resource)
        consumed[key] = r
    return consumed


def generate_resource_cache_keys(exchanges, limit_types=None, intervals=None, current_date=None):
    if not intervals:
        intervals = [i for i in TimeInterval.values()]
    if not limit_types:
        limit_types = [t for t in RateLimitTypes.values()]
    if not current_date:
        current_date = datetime.datetime.utcnow()

    for exchange in exchanges:
        for limit_type in limit_types:
            for interval in intervals:
                if interval == TimeInterval.MINUTE.value:
                    yield exchange+'-'+limit_type+'-'+interval+'-'+str(current_date.minute)
                elif interval == TimeInterval.HOUR.value:
                    yield exchange + '-' + limit_type + '-' + interval + '-' + str(current_date.hour)
                else:
                    yield exchange + '-' + limit_type + '-' + interval + '-' + str(current_date.weekday())
