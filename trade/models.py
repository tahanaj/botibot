from decimal import Decimal
from enum import Enum

# TODO set risk profile automatically depending on the route's gain
# TODO compute minimum allowed gain authomtically, for now 0.02 is calculated according to the 0.5% BNB fees in binance
MINIMUM_ROUTE_GAIN = 'minimum_route_gain'
ROUTE_SIZE = 'route_size'
ORDER_LIFE_TIME = 'order_life_time'
ALLOCATION = 'allocation'

MAX_PARALLEL_ORDERS = 10
DEFAULT_DECIMAL_PRECISION = 28


class RiskProfiles(Enum):
    """
    Helper to all safety switches in trades and arbitrage finding
    """
    LOW = {
        ALLOCATION: Decimal(1)/Decimal(5),
        ORDER_LIFE_TIME: 5000,
        ROUTE_SIZE: 3,
        MINIMUM_ROUTE_GAIN: Decimal(299)/Decimal(10000)
    }

    MODERATE = {
        ALLOCATION: Decimal(2)/Decimal(5),
        ORDER_LIFE_TIME: 2000,
        ROUTE_SIZE: 3,
        MINIMUM_ROUTE_GAIN: Decimal(199)/Decimal(10000)
    }
    HIGH = {
        ALLOCATION: Decimal(2)/Decimal(3),
        ORDER_LIFE_TIME: 5000,
        ROUTE_SIZE: 3,
        MINIMUM_ROUTE_GAIN: Decimal(299)/Decimal(10000)
    }
