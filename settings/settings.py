from __future__ import absolute_import

#set TEST_ENV to True in user_settings.py to test orders (without sending them to the exchange matching engine
TEST_ENV = False

#set DEBUG to true in user_settings.py to get --verbose logging mode
DEBUG = False

binance_key = None
binance_secret = None
RESOURCE_CACHE_URL = '127.0.0.1'
LOG_FILE_PATH = './boti_bot.log'

try:
    from settings.user_settings import *
except ImportError:
    print "No user settings found please create one with the exchanges keys!"